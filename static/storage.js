document.addEventListener('DOMContentLoaded', ready);

function ready() {
  stylizeInputFile();
  loadFileList();
}

function uploadFile() {
  const control = document.getElementById('file');
  if (control.files.length > 0) {
    // FileReader;
    const fileName = control.files[0].name;
    const contentType = control.files[0].type;
    control.files[0]
      .arrayBuffer()
      .then((data) => {
        fetch(`/api/files/${fileName}`, {
          method: 'PUT',
          headers: {
            'Content-Type': contentType,
          },
          body: data,
        })
          .then((r) => {
            if (r.ok) {
              loadFileList();
            } else {
              displayError('Error upload file (');
            }
          })
          .catch((e) => displayError(e.message));
      })
      .finally(() => {
        control.value = '';
        const evt = document.createEvent('HTMLEvents');
        evt.initEvent('change', false, true);
        control.dispatchEvent(evt);
      });
  } else {
    console.log('Select file for upload');
  }
}

function displayLoader() {
  document.getElementById('file-list').innerHTML = '<div class="loader"></div>';
}

function displayError(message) {
  document.getElementById(
    'file-list',
  ).innerHTML = `<div class="error">${message}</div>`;
}
function downloadFile(filename) {
  window.location.href = `/api/files/${filename}`;
}

function deleteFile(filename) {
  const isConfirmed = confirm(`really delete the file ${filename}`);
  if (isConfirmed) {
    fetch(`/api/files/${filename}`, { method: 'DELETE' })
      .then((r) => r.json())
      .then((data) => {
        if (data.status === 'Ok') {
          loadFileList();
        } else {
        }
      })
      .catch((e) => {
        console.error(e.message);
        displayError(e.message);
      });
  }
  return false;
}

function loadFileList() {
  displayLoader();
  fetch('/api/files')
    .then((r) => r.json())
    .then((data) => {
      if (data.status === 'Ok') {
        const container = document.getElementById('file-list');
        container.innerHTML = '';
        if (Array.isArray(data.files) && data.files.length > 0) {
          const content = data.files.map((f) => {
            return `
                <div class="file-list__item">
                    <div class="file-list__item--name">${f.fileName}</div>
                    <div class="file-list__item--size">${getReadableFileSizeString(
                      f.size,
                    )}</div>
                    <div class="file-list__item-actions">
                      <a class="file-list__item-action" href="#" title="Download" onclick="downloadFile('${
                        f.fileName
                      }')">⭳</a>
                      <a class="file-list__item-action" href="#" title="Delete" onclick="deleteFile('${
                        f.fileName
                      }')">⨯</a>
                    </div>
                </div>
           `;
          });
          container.insertAdjacentHTML('beforeend', content.join(''));
        } else {
          container.innerHTML = '<div class="info"> Files not exists</div>';
        }
      }
    })
    .catch((e) => {
      console.error(e.message);
      displayError(e.message);
    });
}

function getReadableFileSizeString(fileSizeInBytes) {
  let i = -1;
  const byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
  do {
    fileSizeInBytes = fileSizeInBytes / 1024;
    i++;
  } while (fileSizeInBytes > 1024);
  return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
}

function stylizeInputFile() {
  const inputs = document.querySelectorAll('.file-upload__input');
  Array.prototype.forEach.call(inputs, function (input) {
    const label = input.nextElementSibling;
    const labelVal = label.innerHTML;

    input.addEventListener('change', function (e) {
      let fileName = '';
      if (this.files && this.files.length > 1) {
        fileName = (this.getAttribute('data-multiple-caption') || '').replace(
          '{count}',
          this.files.length,
        );
      } else {
        fileName = e.target.value.split('\\').pop();
      }

      if (fileName) label.querySelector('span').innerHTML = fileName;
      else label.innerHTML = labelVal;
    });
  });
}
