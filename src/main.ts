import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { AppModule } from './app.module';
import { json } from 'body-parser';

const jsonParseMiddleware = json();

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bodyParser: false });
  app.setGlobalPrefix('/api');
  app.use((req: any, res: any, next: any) => {
    // do not parse json bodies if we are hitting file uploading controller
    if (
      req.path.indexOf('/api/files') === 0 &&
      ['POST', 'PUT'].includes(req.method)
    ) {
      const data = [];
      req.on('data', function (chunk) {
        data.push(chunk);
      });
      req.on('end', function () {
        req.rawData = Buffer.concat(data);
        next();
      });
    } else {
      jsonParseMiddleware(req, res, next);
    }
  });
  const configService = app.get(ConfigService);
  const port = configService.get('port');
  await app.listen(port);
}
bootstrap();
