import { join } from 'path';

export default () => ({
  port: parseInt(process.env.PORT, 10) || 3000,
  storage: {
    warning: parseInt(process.env.SIZE_WARNING, 10) || 100000000,
    path: (fileName: string): string =>
      join(process.cwd(), process.env.FOLDER || 'data', fileName),
  },
});
