import { Test, TestingModule } from '@nestjs/testing';
import { StorageController } from './storage.controller';
import { FileMeta } from './models/file-meta.entity';
import { ConfigService } from '@nestjs/config';
import { MetaSqliteService } from './meta-sqlite/meta-sqlite.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { LocalAdapterService } from './local-adapter/local-adapter.service';

describe('StorageController', () => {
  let controller: StorageController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ConfigService,
        MetaSqliteService,
        LocalAdapterService,
        {
          provide: getRepositoryToken(FileMeta),
          useValue: {
            save: jest.fn().mockResolvedValue(true),
            find: jest.fn().mockResolvedValue([]),
          },
        },
      ],
      controllers: [StorageController],
    }).compile();

    controller = module.get<StorageController>(StorageController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
