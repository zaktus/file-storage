import { StreamableFile } from '@nestjs/common';

export interface IStorageAdapter {
  read(fileName: string): StreamableFile;
  save(fileName: string, buffer: Buffer): boolean;
  delete(fileName: string): boolean | Promise<boolean>;
}
