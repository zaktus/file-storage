import { FileMeta } from '../models/file-meta.entity';
import { DeleteResult } from 'typeorm';

export interface IStorageMeta {
  findOne(fileName: string): Promise<FileMeta>;
  findAll(): Promise<FileMeta[]>;
  create(meta: FileMeta): Promise<FileMeta>;
  delete(fileName: string): Promise<DeleteResult>;
  getTotalSize(): Promise<number>;
}
