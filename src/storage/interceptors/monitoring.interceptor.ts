import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  Logger,
  Inject,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { MetaSqliteService } from '../meta-sqlite/meta-sqlite.service';
import { IStorageMeta } from '../interfaces/storage-meta.interface';
import { ConfigService } from '@nestjs/config';
import readableBytes from 'readable-bytes/lib';

@Injectable()
export class MonitoringInterceptor implements NestInterceptor {
  constructor(
    @Inject(MetaSqliteService) private metaService: IStorageMeta,
    private configService: ConfigService,
  ) {}
  private readonly logger = new Logger('STORAGE');
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    // get path and method
    const request: Request = context.switchToHttp().getRequest();
    const path = request.url;
    const method = request.method;
    // this.logger.log(`***** ${path} ****** ${headers['content-length']}`);

    if (['PUT'].includes(method)) {
      this.logger.log(`Start write file ${path} ...`);
      const now = Date.now();
      return next.handle().pipe(
        tap(async () => {
          this.logger.log(`End write file ${Date.now() - now}ms`);
          const warningSize = this.configService.get<number>('storage.warning');
          const totalSize = await this.metaService.getTotalSize();
          if (totalSize > warningSize) {
            this.logger.warn(
              `Total size of folder ${readableBytes(totalSize)}`,
            );
          }
        }),
      );
    }
    return next.handle();
  }
}
