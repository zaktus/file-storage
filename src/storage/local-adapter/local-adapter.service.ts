import { Injectable, StreamableFile } from '@nestjs/common';
import { IStorageAdapter } from '../interfaces/storage-adapter.interface';
import { createReadStream } from 'fs';
import * as fs from 'fs';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class LocalAdapterService implements IStorageAdapter {
  private readonly getPath: (string) => string;

  constructor(private configService: ConfigService) {
    this.getPath = configService.get<(string) => string>('storage.path');
  }

  read(fileName: string): StreamableFile {
    const file = createReadStream(this.getPath(fileName));
    return new StreamableFile(file);
  }

  save(fileName: string, buffer: Buffer): boolean {
    const ws = fs.createWriteStream(this.getPath(fileName), {
      flags: 'w+',
      encoding: 'binary',
    });
    ws.write(buffer);
    ws.end();
    return true;
  }

  async delete(fileName: string): Promise<boolean> {
    try {
      await fs.promises.unlink(this.getPath(fileName));
      return true;
    } catch (err) {
      return false;
    }
  }
}
