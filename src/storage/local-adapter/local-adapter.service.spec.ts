import { Test, TestingModule } from '@nestjs/testing';
import { LocalAdapterService } from './local-adapter.service';
import { ConfigService } from '@nestjs/config';

describe('LocalAdapterService', () => {
  let service: LocalAdapterService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LocalAdapterService, ConfigService],
    }).compile();

    service = module.get<LocalAdapterService>(LocalAdapterService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
