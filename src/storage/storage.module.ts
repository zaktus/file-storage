import { Module } from '@nestjs/common';
import { StorageController } from './storage.controller';
import { LocalAdapterService } from './local-adapter/local-adapter.service';
import { FileMeta } from './models/file-meta.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MetaSqliteService } from './meta-sqlite/meta-sqlite.service';

@Module({
  imports: [TypeOrmModule.forFeature([FileMeta])],
  controllers: [StorageController],
  providers: [LocalAdapterService, MetaSqliteService],
})
export class StorageModule {}
