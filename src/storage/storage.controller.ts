import {
  Controller,
  Get,
  Inject,
  NotFoundException,
  Param,
  Req,
  Response,
  Request,
  StreamableFile,
  InternalServerErrorException,
  Put,
  Logger,
  Delete,
  UseInterceptors,
} from '@nestjs/common';

import { IStorageAdapter } from './interfaces/storage-adapter.interface';
import { LocalAdapterService } from './local-adapter/local-adapter.service';
import { FileMeta } from './models/file-meta.entity';
import { MetaSqliteService } from './meta-sqlite/meta-sqlite.service';
import { IStorageMeta } from './interfaces/storage-meta.interface';
import readableBytes from 'readable-bytes';
import { MonitoringInterceptor } from './interceptors/monitoring.interceptor';

@Controller('files')
@UseInterceptors(MonitoringInterceptor)
export class StorageController {
  private readonly logger = new Logger('STORAGE');

  constructor(
    @Inject(LocalAdapterService) private adapterService: IStorageAdapter,
    @Inject(MetaSqliteService) private metaService: IStorageMeta,
  ) {}

  @Get()
  async getFiles() {
    const files = await this.metaService.findAll();
    const size = await this.metaService.getTotalSize();
    return { status: 'Ok', files, size: readableBytes(size) };
  }

  @Get(':fileName')
  async getFile(
    @Param('fileName') fileName: string,
    @Response({ passthrough: true }) res,
  ): Promise<StreamableFile> {
    const meta: FileMeta = await this.metaService.findOne(fileName);
    if (meta) {
      res.set({
        'Content-Type': meta.mimeType,
        'Content-Disposition': `attachment; filename="${meta.fileName}"`,
        'Content-Length': meta.size,
      });
      return this.adapterService.read(meta.fileName);
    } else {
      throw new NotFoundException('File not found');
    }
  }

  @Put(':fileName')
  async uploadFile(
    @Param('fileName') fileName: string,
    @Req() request: Request,
  ) {
    const meta: FileMeta = new FileMeta();
    meta.fileName = fileName;
    meta.mimeType = request.headers['content-type'];
    meta.size = request.headers['content-length'];
    const buffer = request['rawData'];
    if (this.adapterService.save(fileName, buffer)) {
      return await this.metaService.create(meta);
    } else {
      this.logger.error(`Error save file ${fileName} (${meta.size})`);
      throw new InternalServerErrorException('Error save file');
    }
  }

  @Delete(':fileName')
  async delete(@Param('fileName') fileName: string) {
    const meta = await this.metaService.findOne(fileName);
    if (meta) {
      if (this.adapterService.delete(fileName)) {
        await this.metaService.delete(fileName);
        this.logger.log(`File ${fileName} is deleted`);
        return { status: 'Ok' };
      } else {
        this.logger.error(`Can't delete file ${fileName}`);
        throw new InternalServerErrorException(`Can't delete file ${fileName}`);
      }
    } else {
      throw new NotFoundException(`File ${fileName} not found!`);
    }
  }
}
