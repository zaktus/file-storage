import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity()
export class FileMeta {
  @PrimaryColumn()
  fileName: string;

  @Column()
  mimeType: string;

  @Column()
  size: number;
}
