import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { FileMeta } from '../models/file-meta.entity';
import { IStorageMeta } from '../interfaces/storage-meta.interface';

@Injectable()
export class MetaSqliteService implements IStorageMeta {
  constructor(
    @InjectRepository(FileMeta)
    private FileMetaRepository: Repository<FileMeta>,
  ) {}
  /*
   * Return all records
   */
  async findAll(): Promise<FileMeta[]> {
    return await this.FileMetaRepository.find();
  }

  /*
   * Find meta by file name
   */
  async findOne(fileName: string): Promise<FileMeta> {
    return await this.FileMetaRepository.findOne({ where: { fileName } });
  }

  /*
   * Create or update meta
   */
  async create(meta: FileMeta): Promise<FileMeta> {
    return await this.FileMetaRepository.save(meta);
  }

  /*
   * Delete  meta
   */
  async delete(fileName: string): Promise<DeleteResult> {
    return await this.FileMetaRepository.delete({ fileName });
  }

  async getTotalSize(): Promise<number> {
    const total = await this.FileMetaRepository.createQueryBuilder('meta')
      .select('SUM(meta.size)', 'size')
      .getRawOne();
    return total.size;
  }
}
