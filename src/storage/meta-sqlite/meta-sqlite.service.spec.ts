import { Test, TestingModule } from '@nestjs/testing';
import { MetaSqliteService } from './meta-sqlite.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { FileMeta } from '../models/file-meta.entity';

describe('MetaSqliteService', () => {
  let service: MetaSqliteService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MetaSqliteService,
        {
          provide: getRepositoryToken(FileMeta),
          useValue: {
            save: jest.fn().mockResolvedValue(true),
            find: jest.fn().mockResolvedValue([]),
          },
        },
      ],
    }).compile();

    service = module.get<MetaSqliteService>(MetaSqliteService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
